package com.example.lab1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.RadioButton
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun showMessage(view: View){
        val builder = AlertDialog.Builder(this);
        val checkedRadioButtonId = radioGroup.checkedRadioButtonId;
        val checkedRadioButton = findViewById<RadioButton>(checkedRadioButtonId);
        val checkedRadioButtonText = checkedRadioButton.text;
        if(checkedRadioButtonText=="Вашингтон") {
            builder.setMessage("Вы ответили правильно! Столица США - Вашингтон!");
        }
        else{
            builder.setMessage("Неверно! $checkedRadioButtonText не столица США!");
        }
        builder.show();
    }
}
