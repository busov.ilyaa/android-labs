package com.example.lab2

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class MainActivity : AppCompatActivity(), QuizFragment.OnFragmentInteractionListener {
    override fun onFragmentInteraction(str: String) {
        val blankFragment = supportFragmentManager.findFragmentById(R.id.fragmentBlank) as BlankFragment
        if(blankFragment != null && blankFragment.isInLayout){
            blankFragment.showMessage(str)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
