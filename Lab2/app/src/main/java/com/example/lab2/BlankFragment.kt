package com.example.lab2

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.fragment_blank.*
import kotlinx.android.synthetic.main.fragment_quiz.*


class BlankFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_blank, container, false)
    }


    fun showMessage(str: String){
        val textView = getView()!!.findViewById<TextView>(R.id.textView)
        if(str == "Вашингтон"){
            textView.text = "Вы выбрали правильный ответ! Столица США - $str"
        }
        else{
            textView.text = "$str не столица США!"
        }

    }
}