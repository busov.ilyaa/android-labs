package com.example.lab2

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.fragment_quiz.*


class QuizFragment : Fragment() {
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_quiz, container, false);
        // Inflate the layout for this fragment
        val button = view!!.findViewById<Button>(R.id.button)
        button.setOnClickListener {
                showAnswer()
        }
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(str: String)
    }

    fun showAnswer(){
        val checkedRadioButtonId = view!!.findViewById<RadioGroup>(R.id.radioGroup).checkedRadioButtonId
        val checkedRadioButton = view!!.findViewById<RadioButton>(checkedRadioButtonId)
        val answer: String = checkedRadioButton.text.toString();
        listener?.onFragmentInteraction(answer)
    }
}
