package com.example.lab4
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.net.Uri
import android.widget.VideoView
import android.R.attr.start
import android.view.View
import android.content.Intent
import android.R.attr.data
import android.app.Activity
import kotlinx.android.synthetic.main.activity_main.*
import android.media.MediaPlayer
import android.R.attr.start
import android.app.ProgressDialog


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun open(view: View){
        val photoPickerIntent = Intent(Intent.ACTION_PICK)
        photoPickerIntent.type = "video/*"
        startActivityForResult(photoPickerIntent, 1)
    }

    fun play(view: View) {
        videoPlayer.start()
    }

    fun pause(view: View) {
        videoPlayer.pause()
    }

    fun playLink(view: View){
        val pd = ProgressDialog(this)
        pd.setMessage("Buffering video please wait...")
        pd.show()

        val uri = Uri.parse(linkText.text.toString())
        videoPlayer.setVideoURI(uri)

        videoPlayer.setOnPreparedListener({
            pd.dismiss()
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode === Activity.RESULT_OK) {
            if (data != null && data.data != null) {
                val fileUri = data.data
                videoPlayer.setVideoURI(fileUri)
            }
        }
    }

    fun stop(view: View) {
        videoPlayer.stopPlayback()
        videoPlayer.resume()
    }
}
