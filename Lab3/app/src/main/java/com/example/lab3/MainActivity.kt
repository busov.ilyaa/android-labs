package com.example.lab3

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.View
import java.io.FileInputStream
import java.io.IOException
import android.content.Context
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import java.io.FileOutputStream
import java.nio.charset.Charset


class MainActivity : AppCompatActivity() {
    val dataFileName:String = "data.txt"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        saveButton.setOnClickListener({
            saveAnswer()
        })

        readButton.setOnClickListener({
            loadAnswers()
        })

        cleanDataButton.setOnClickListener({
            deleteSavedFile()
        })
    }

    fun checkAnswers(view: View){
        val radioGroup1 = findViewById<RadioGroup>(R.id.radioGroup1)
        val radioGroup2 = findViewById<RadioGroup>(R.id.radioGroup2)
        val radioGroup3 = findViewById<RadioGroup>(R.id.radioGroup3)

        val answerId1 = radioGroup1.checkedRadioButtonId
        val answerId2= radioGroup2.checkedRadioButtonId
        val answerId3 = radioGroup3.checkedRadioButtonId

        val answer1 = findViewById<RadioButton>(answerId1)
        val answer2= findViewById<RadioButton>(answerId2)
        val answer3 = findViewById<RadioButton>(answerId3)

        if(answer1.text=="pi*R^2" && answer2.text=="8" && answer3.text=="Эмманюэль Макрон"){
            AlertDialog.Builder(this).setMessage("Вы правильно ответили на все вопросы!").show()
        }
        else if (answer1.text!="pi*R^2"){
            AlertDialog.Builder(this).setMessage("Вы неправильно ответили на вопрос 1!").show()
        }
        else if(answer2.text!="8"){
            AlertDialog.Builder(this).setMessage("Вы неправильно ответили на вопрос 2!").show()
        }
        else if(answer3.text!="Эмманюэль Макрон"){
            AlertDialog.Builder(this).setMessage("Вы неправильно ответили на вопрос 3!").show()
        }
    }

    fun saveAnswer(){
        val radioGroup1 = findViewById<RadioGroup>(R.id.radioGroup1)
        val radioGroup2 = findViewById<RadioGroup>(R.id.radioGroup2)
        val radioGroup3 = findViewById<RadioGroup>(R.id.radioGroup3)

        val checkedIndex1 = radioGroup1.indexOfChild(findViewById<RadioButton>(radioGroup1.checkedRadioButtonId))
        val checkedIndex2 = radioGroup2.indexOfChild(findViewById<RadioButton>(radioGroup2.checkedRadioButtonId))
        val checkedIndex3 = radioGroup3.indexOfChild(findViewById<RadioButton>(radioGroup3.checkedRadioButtonId))

        val textToSave = "$checkedIndex1\n$checkedIndex2\n$checkedIndex3"

        var fos: FileOutputStream? = null
        try {
            fos = openFileOutput(dataFileName, Context.MODE_PRIVATE)
            fos.write(textToSave.toByteArray())
            Toast.makeText(this, "Ответы сохранены", Toast.LENGTH_SHORT).show()
        } catch (ex: IOException) {
            Toast.makeText(this, ex.message, Toast.LENGTH_SHORT).show()
        }
        finally{
            try{
                fos?.close()
            }
            catch(ex: IOException){

                Toast.makeText(this, ex.message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    fun loadAnswers(){
        var fin: FileInputStream? = null
        try {
            fin = openFileInput(dataFileName)
            val text = String(fin.readBytes(), Charset.defaultCharset())
            checkRadioButtons(text.split('\n'))
        }
        catch(ex: IOException) {
            Toast.makeText(this, "Нет сохраненных данных!", Toast.LENGTH_SHORT).show()
            checkRadioButtons("0\n0\n0".split('\n'))
        }
        finally{
            try{
                fin?.close()
            }
            catch(ex: IOException){

                Toast.makeText(this, ex.message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    fun checkRadioButtons(ids: List<String>){
        val idInt1 = ids[0].toInt()
        val idInt2 = ids[1].toInt()
        val idInt3 = ids[2].toInt()

        (radioGroup1.getChildAt(idInt1) as RadioButton).isChecked = true
        (radioGroup2.getChildAt(idInt2) as RadioButton).isChecked = true
        (radioGroup3.getChildAt(idInt3) as RadioButton).isChecked = true
    }

    fun deleteSavedFile(){
        deleteFile(dataFileName)
    }
}
